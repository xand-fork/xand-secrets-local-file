use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, error::Error, fs::File};
use xand_secrets::{CheckHealthError, ReadSecretError, Secret, SecretKeyValueStore};

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct LocalFileSecretStoreConfiguration {
    pub yaml_file_path: String,
}

pub struct LocalFileSecretKeyValueStore {
    config: LocalFileSecretStoreConfiguration,
}

impl LocalFileSecretKeyValueStore {
    pub fn create_from_config(config: LocalFileSecretStoreConfiguration) -> Self {
        LocalFileSecretKeyValueStore { config }
    }
}

fn request_error_from(e: impl Error + Send + Sync + 'static) -> ReadSecretError {
    ReadSecretError::Request {
        internal_error: Box::new(e),
    }
}

#[async_trait]
impl SecretKeyValueStore for LocalFileSecretKeyValueStore {
    async fn read(&self, key: &str) -> Result<Secret<String>, ReadSecretError> {
        let file = File::open(self.config.yaml_file_path.as_str()).map_err(request_error_from)?;
        let key_map: HashMap<String, String> =
            serde_yaml::from_reader(file).map_err(request_error_from)?;

        key_map
            .get(key)
            .map(|v| Secret::new(v.clone()))
            .ok_or(ReadSecretError::KeyNotFound {
                key: String::from(key),
            })
    }

    async fn check_health(&self) -> Result<(), CheckHealthError> {
        let file = File::open(self.config.yaml_file_path.as_str()).map_err(|e| {
            CheckHealthError::Unreachable {
                internal_error: Box::new(e),
            }
        })?;
        let _key_map: HashMap<String, String> =
            serde_yaml::from_reader(file).map_err(|e| CheckHealthError::RemoteInternal {
                internal_error: Box::new(e),
            })?;

        Ok(())
    }
}
