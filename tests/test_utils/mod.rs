use std::io::Write;
use tempfile::NamedTempFile;
use xand_secrets_local_file::LocalFileSecretStoreConfiguration;

pub fn file_with_contents(content: &str) -> NamedTempFile {
    let mut file = NamedTempFile::new().unwrap();
    file.as_file_mut().write_all(content.as_bytes()).unwrap();

    file
}

pub fn config_for_tempfile(file: &NamedTempFile) -> LocalFileSecretStoreConfiguration {
    LocalFileSecretStoreConfiguration {
        yaml_file_path: String::from(file.path().to_str().unwrap()),
    }
}
